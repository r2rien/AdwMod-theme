<img src=".data/logo.svg" alt="AdwMod" align="left" width="64" height="64"/>

# AdwMod (overide)

AdwMod (**Adw**aita **Mod**ified, formerly AdwaitaExtended) is a Adwaita (GNOME/GTK default theme) modified with some enhancements and additions.

With some overides for strict selected bg/fg color states,
Less animations for my not so new hardware,
Mainly targeted toward compact light theme, more greyish/blueish with sidebar color variations and inset shadow/rounding (like gtk4 rounded windows !)
... as a new attempt to catchup with gnome theming,
to understand gtk4 (not tested) changes again
and as a farewell to https://www.deviantart.com/r2rien

## Screenshots
![Light_1](/.data/light_1.png)
![Light_2](/.data/light_2.png)
![Light_3](/.data/light_3.png)
![Dark_1](/.data/dark_1.png)
![Dark_2](/.data/dark_2.png)
![Dark_3](/.data/dark_3.png)

## Recommendations
### Font
- Any `sans-serif` font (`Fira-sans` in screenshots)
- font size: 12pt for **compact** variant

## Credits
- https://github.com/hrdwrrsk/AdwaitaExtended - hrdwrrsk
- https://gitlab.gnome.org/GNOME/gtk - Adwaita GTK3 theme
- https://gitlab.gnome.org/GNOME/libadwaita - Adwaita GTK4 theme
- https://gitlab.gnome.org/GNOME/gnome-themes-extra - Adwaita GTK2 theme
- https://gitlab.gnome.org/GNOME/gnome-shell - Adwaita gnome-shell theme
- https://github.com/ubuntu/yaru - upstream sync script
- https://github.com/nana-4/materia-theme - meson.build files including gnome-shell version detect logic
