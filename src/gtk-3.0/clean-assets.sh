#! /bin/bash

INDEX="assets.txt"

for i in `cat $INDEX`
do
    printf "$i.png "
    if [ ! -f assets/$i.png ]; then
        echo "Skipped"
        continue
    else
        rm -f assets/$i.png && echo " Deleted"
    fi
    printf "$i@2.png "
    if [ ! -f assets/$i@2.png ]; then
        echo "Skipped"
        continue
    else
        rm -f assets/$i@2.png && echo " Deleted"
    fi
done
exit 0
